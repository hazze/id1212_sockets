package andre.se.kth.id1212.server.net;

import andre.se.kth.id1212.common.Message;
import andre.se.kth.id1212.common.MsgType;
import andre.se.kth.id1212.server.controller.Controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UncheckedIOException;
import java.net.Socket;

/**
 * Handles all the different client connections. All messages received from and sent to clients pass through here
 */
class ClientHandler implements Runnable {
    private final GameServer server;
    private final Socket clientSocket;
    private ObjectInputStream fromClient;
    private ObjectOutputStream toClient;
    private Controller controller;
    private boolean connected;

    /**
     * Creates an instance of the client handler
     * @param server Reference to the server the client connected to.
     * @param clientSocket Reference to the socket the client used to connect.
     * @param controller Reference to the server side controller.
     */
    ClientHandler(GameServer server, Socket clientSocket, Controller controller) {
        this.server = server;
        this.clientSocket = clientSocket;
        this.controller = controller;
        connected = true;
    }

    /**
     * The run loop handling all the communication with the client.
     */
    @Override
    public void run() {
        try {
            fromClient = new ObjectInputStream(clientSocket.getInputStream());
            toClient = new ObjectOutputStream(clientSocket.getOutputStream());
        } catch (IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
        while (connected) {
            try {
                sendMsg(controller.parseMessage((Message) fromClient.readObject()));
            }
            catch (IOException | ClassNotFoundException e) {
                disconnectClient();
                System.out.println("Client disconnected.");
            }
        }
    }

    private void sendMsg(Message msg) throws UncheckedIOException {
        try {
            toClient.writeObject(msg);
            toClient.flush();
            toClient.reset();
            if (msg.getType() == MsgType.DISCONNECT)
                disconnectClient();
        } catch (IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    private void disconnectClient() {
        try {
            clientSocket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        connected = false;
        server.removeHandler(this);
    }
}
