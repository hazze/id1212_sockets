package andre.se.kth.id1212.common;

/**
 * Enum for the different typ of messages
 */
public enum MsgType {
    START,
    DISCONNECT,
    LETTER,
    WORD,
    GAME,
}
