package andre.se.kth.id1212.client.view;


/**
 * Class to provide a thread safe output.
 * Methods for printing are synchronized.
 */
class ThreadSafeStdOut {
    /**
     * Prints the text without line break.
     * @param output Text to print
     */
    synchronized void print(String output) {
        System.out.print(output);
    }

    /**
     * Prints the text with line break.
     * @param output Text to print
     */
    synchronized void println(String output) {
        System.out.println(output);
    }
}
