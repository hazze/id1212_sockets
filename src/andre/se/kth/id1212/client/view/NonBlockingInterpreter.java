package andre.se.kth.id1212.client.view;

import andre.se.kth.id1212.client.controller.Controller;
import andre.se.kth.id1212.client.net.OutputHandler;
import andre.se.kth.id1212.common.MsgType;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Reads and interprets user commands. The command interpreter will run in a separate thread, which
 * is started by calling the <code>start</code> method. Commands are executed in a thread pool, a
 * new prompt will be displayed as soon as a command is submitted to the pool, without waiting for
 * command execution to complete.
 */
public class NonBlockingInterpreter implements Runnable {
    private static final String PROMPT = "> ";
    private static final String COMMANDS = "S -> New game | 'L <character>' -> Guess character | 'W <word>' -> Guess word | Q -> Disconnect";
    private final Scanner console = new Scanner(System.in);
    private boolean receivingCmds = false;
    private Controller controller;
    private final ThreadSafeStdOut outMgr = new ThreadSafeStdOut();

    /**
     * Starts the interpreter. The interpreter will be waiting for user input when this method
     * returns.
     */
    public void start() {
        if (receivingCmds) {
            return;
        }
        receivingCmds = true;
        controller = new Controller();
        controller.connect("127.0.0.1", 4444, new ConsoleOutput());
        new Thread(this).start();
    }

    /**
     * Interprets the user commands and performs actions accordingly.
     */
    @Override
    public void run() {
        while (receivingCmds) {
            try {
                System.out.print(PROMPT);
                receivingCmds = controller.parseCommand(console.nextLine());
            }
            catch (Exception e) {
                outMgr.println("Operation failed");
            }
        }
    }

    private class ConsoleOutput implements OutputHandler {
        /**
         * Handles messages for output sent from the server connection.
         * @param msg Reference to the message.
         */
        @Override
        public void handleMsg(String msg) {
            outMgr.println(msg);
            outMgr.println(COMMANDS);
            outMgr.print(PROMPT);
        }

        /**
         * Handles game messages for output sent from the server connection.
         * @param msg Reference to the message.
         */
        public void handleGameMsg(ArrayList<String> msg) {
            for (String print : msg) {
                outMgr.println(print);
            }
            outMgr.println(COMMANDS);
            outMgr.print(PROMPT);
        }
    }
}
