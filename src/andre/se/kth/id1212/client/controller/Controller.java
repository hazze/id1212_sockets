package andre.se.kth.id1212.client.controller;

import andre.se.kth.id1212.client.net.OutputHandler;
import andre.se.kth.id1212.client.net.ServerConnection;
import andre.se.kth.id1212.common.MsgType;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.CompletableFuture;

/**
 * The client side controller.
 */
public class Controller {
    private final ServerConnection serverConnection = new ServerConnection();

    /**
     * Calls the serverConnection.connect to set up new connection.
     * @param host The ip to use to connect to the host.
     * @param port The port to use to connect to the host.
     * @param outputHandler Called whenever a message is received from the server and needs to be printed.
     */
    public void connect(String host, int port, OutputHandler outputHandler) {
        CompletableFuture.runAsync(() -> {
            try {
                serverConnection.connect(host, port, outputHandler);
            } catch (IOException ioe) {
                throw new UncheckedIOException(ioe);
            }
        }).thenRun(() -> outputHandler.handleMsg("Connected to " + host + ":" + port));
    }

    private void disconnect() throws IOException {
        serverConnection.disconnect();
    }

    /**
     * Function to parse message.
     * @param command Command to parse
     * @return true if view should continue receive commands or false if not.
     */
    public boolean parseCommand(String command) throws IOException {
        switch (command.toUpperCase().charAt(0)) {
            case 'S':
                sendMsg(MsgType.START, null);
                return true;
            case 'L':
                sendMsg(MsgType.LETTER, command);
                return true;
            case 'W':
                sendMsg(MsgType.WORD, command);
                return true;
            case 'Q':
                sendMsg(MsgType.DISCONNECT, null);
                disconnect();
                return false;
            default:
                System.out.println("Wrong command, try again.");
                return true;
        }
    }

    private void sendMsg(MsgType type, String msg) {
        CompletableFuture.runAsync(() -> {
            try {
                switch (type) {
                    case START:
                        serverConnection.requestNewGame();
                        break;
                    case LETTER:
                        serverConnection.sendCharacterGuess(String.valueOf(msg.charAt(2)));
                        break;
                    case WORD:
                        serverConnection.sendWordGuess(msg.substring(2));
                        break;
                    case DISCONNECT:
                        serverConnection.disconnect();
                        break;
                }
            } catch (IOException ioe) {
                throw new UncheckedIOException(ioe);
            }
        });
    }
}