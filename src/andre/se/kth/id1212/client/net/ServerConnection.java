package andre.se.kth.id1212.client.net;

import andre.se.kth.id1212.common.Message;
import andre.se.kth.id1212.common.MsgType;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Manages all communicating with the server.
 */
public class ServerConnection {
    private static final int TIMEOUT_HALF_HOUR = 1800000;
    private static final int TIMEOUT_HALF_MINUTE = 30000;
    private Socket socket;
    private ObjectOutputStream toServer;
    private ObjectInputStream fromServer;
    private boolean connected;

    /**
     * Sets up the connection to the server
     * @param host The ip for connecting to the host.
     * @param port The port to use when connecting
     * @param messageHandler Reference to the class that handles the incoming server messages.
     * @throws IOException If the connection fails.
     */
    public void connect(String host, int port, OutputHandler messageHandler) throws
            IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, port), TIMEOUT_HALF_MINUTE);
        socket.setSoTimeout(TIMEOUT_HALF_HOUR);
        connected = true;
        toServer = new ObjectOutputStream(socket.getOutputStream());
        fromServer = new ObjectInputStream(socket.getInputStream());
        new Thread(new Listener(messageHandler)).start();
    }

    /**
     * Sends disconnect messages to the server and closes the connection.
     * @throws IOException If sending the message fails.
     */
    public void disconnect() throws IOException {
        sendMsg(MsgType.DISCONNECT, null);
        socket.close();
        socket = null;
        connected = false;
    }

    /**
     * Requests new game to start.
     * @throws IOException If sending the message fails.
     */
    public void requestNewGame() throws IOException {
        sendMsg(MsgType.START, null);
    }

    /**
     * Sends a word guess for a word.
     * @param msg The word to guess.
     * @throws IOException If sending message fails.
     */
    public void sendWordGuess(String msg) throws IOException {
        sendMsg(MsgType.WORD, msg);
    }

    /**
     * Sends a character guess for a word.
     * @param msg The character to guess
     * @throws IOException If sending message fails.
     */
    public void sendCharacterGuess(String msg) throws IOException {
        sendMsg(MsgType.LETTER, msg);
    }

    private void sendMsg(MsgType type, String body) throws IOException {
        Message msg = new Message(type, body, null);
        toServer.writeObject(msg);
        toServer.flush();
        toServer.reset();
    }

    /**
     * Listener for incoming server messages.
     */
    private class Listener implements Runnable {
        private final OutputHandler outputHandler;

        private Listener(OutputHandler outputHandler) {
            this.outputHandler = outputHandler;
        }

        /**
         * Runnable function that loops and listens for messages.
         */
        @Override
        public void run() {
            try {
                for (;;) {
                    outputHandler.handleGameMsg(handleIncMsg((Message) fromServer.readObject()));
                }
            } catch (Exception connectionFailure) {
                if (connected) {
                    connectionFailure.printStackTrace();
                    outputHandler.handleMsg("Lost connection.");
                }
            }
        }

        private ArrayList<String> handleIncMsg(Message msg) {
            ArrayList<String> msgArray = new ArrayList<>();
            switch (msg.getType()) {
                case GAME:
                    switch (msg.getGameState().getState()) {
                        case GAMEOVER:
                            msgArray.add("GAME OVER! The word was " + msg.getGameState().getCorrectLetters());
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore());
                            msgArray.add("Type 'S' to start new game.");
                            return msgArray;
                        case VICTORY:
                            msgArray.add("CORRECT! The word was " + msg.getGameState().getCorrectLetters());
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore());
                            msgArray.add("Type 'S' to start new game.");
                            return msgArray;
                        case INPROGRESS:
                            StringBuilder correct = new StringBuilder(msg.getGameState().getCorrectLetters());
                            for (int i = 0; i < correct.length(); i++)
                                if (i % 2 == 1) correct.insert(i, ' ');
                            msgArray.add("CURRENT GAME: ");
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore() + "  |  Attempts left: " + msg.getGameState().getAttemptsLeft());
                            msgArray.add("Guessed characters: " + msg.getGameState().getGuessedLetters());
                            msgArray.add(correct.toString());
                            msgArray.add(" ");
                            return msgArray;
                    }

                case DISCONNECT:
                    msgArray.add("SERVER SENT DISCONNECT");
                    return msgArray;
            }
            return msgArray;
        }

    }
}