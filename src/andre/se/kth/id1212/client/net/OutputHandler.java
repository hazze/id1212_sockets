package andre.se.kth.id1212.client.net;

import java.util.ArrayList;

/**
 * Handles messages from the server
 */
public interface OutputHandler {
    /**
     * Handles messages from the server.
     * @param msg Message to print.
     */
    void handleMsg(String msg);

    /**
     * Handles game messages from the server.
     * @param msg Message to print.
     */
    void handleGameMsg(ArrayList<String> msg);
}
