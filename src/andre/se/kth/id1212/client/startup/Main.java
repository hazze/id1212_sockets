package andre.se.kth.id1212.client.startup;

import andre.se.kth.id1212.client.view.NonBlockingInterpreter;

/**
 * Starts the client.
 */
public class Main {
    /**
     * @param args String of commandline arguments (NOT USED).
     */
    public static void main(String[] args) {
        new NonBlockingInterpreter().start();
    }
}
